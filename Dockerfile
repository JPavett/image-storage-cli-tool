FROM php:8.1.1-fpm as base

# Install App Dependencies
RUN mkdir -p /usr/share/man/man1 && apt-get --allow-releaseinfo-change update && \
    apt-get install -y -q --no-install-recommends gcc make supervisor \
    nano apt-utils curl unzip default-mysql-client build-essential git \
    libcurl4-gnutls-dev libmcrypt-dev libmagickwand-dev \
    libwebp-dev libjpeg-dev libpng-dev libxpm-dev \
    libfreetype6-dev libaio-dev zlib1g-dev libzip-dev pdftk openssh-client && \
    echo 'umask 002' >> /root/.bashrc  && \
    apt-get clean

# Docker PHP Extensions
RUN docker-php-ext-install -j$(nproc) iconv gettext gd mysqli curl pdo pdo_mysql zip pcntl && \
    docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ && \
    docker-php-ext-configure curl --with-curl && \
    docker-php-ext-configure pcntl --enable-pcntl

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV PATH /var/www/html/vendor/bin:/root/.composer/vendor/bin:$PATH

# Install and configure Node
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash
RUN apt-get -y install nodejs

RUN npm -g config set user root
RUN npm -g install secure-spreadsheet

RUN pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    echo "xdebug.mode=develop,debug" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.client_port=9009" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.log=/var/log/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.log_level=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/xdebug.ini \

COPY ./webroot /var/www/html

ENV PHP_IDE_CONFIG 'serverName=DockerApp'

EXPOSE 80
EXPOSE 9000
EXPOSE 9009
