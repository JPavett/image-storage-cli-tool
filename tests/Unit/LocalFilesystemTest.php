<?php

use App\Adapters\LocalAdapter;
use App\Filesystem;

$filesystem = new Filesystem(new LocalAdapter);

$fileLocation        = '/var/www/html/storage/test.txt';
$destinationLocation = '/var/www/html/storage/new-test.txt';

afterAll(function () use ($fileLocation, $destinationLocation) {
    if (file_exists($fileLocation)) {
        unlink($fileLocation);
    }
    if (file_exists($destinationLocation)) {
        unlink($destinationLocation);
    }
});

it('can check if file exists', function () use ($filesystem, $fileLocation) {
    touch($fileLocation);

    $this->assertTrue($filesystem->fileExists($fileLocation));
    $this->assertFileExists($fileLocation);
});

it('can copy file', function () use ($filesystem, $fileLocation, $destinationLocation) {
    touch($fileLocation);

    $this->assertTrue($filesystem->copyFile($fileLocation, $destinationLocation));
    $this->assertFileExists($fileLocation);
    $this->assertFileExists($destinationLocation);
});

it('can move file', function () use ($filesystem, $fileLocation, $destinationLocation) {
    touch($fileLocation);

    $this->assertTrue($filesystem->moveFile($fileLocation, $destinationLocation));
    $this->assertFileDoesNotExist($fileLocation);
    $this->assertFileExists($destinationLocation);
});

it('can delete file', function () use ($filesystem, $fileLocation) {
    touch($fileLocation);

    $this->assertTrue($filesystem->deleteFile($fileLocation));
    $this->assertFileDoesNotExist($fileLocation);
});

