<?php

use App\Support\FileValidator;

$imageLocation = '/var/www/html/storage/tests/images/TestImage.png';

it('allows valid image mime types', function () use ($imageLocation) {
    $this->assertFileExists($imageLocation);
    $this->assertTrue((new FileValidator())->isAllowedImageType($imageLocation));
});

it('rejects invalid image mime types', function () use ($imageLocation) {
    $invalidFileLocation = '/var/www/html/storage/tests/TestTextFile.txt';

    touch($invalidFileLocation);

    $this->assertFileExists($invalidFileLocation);
    $this->assertFalse((new FileValidator())->isAllowedImageType($invalidFileLocation));

    unlink($invalidFileLocation);
});
