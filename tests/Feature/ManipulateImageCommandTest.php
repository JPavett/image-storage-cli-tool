<?php

test('store command is run', function () {
    $this->artisan('manipulate-image')
         ->expectsQuestion('What would you like to do?', 'Store an Image')
         ->assertExitCode(0);

    $this->assertCommandCalled('manipulate-image:store');
});

test('retrieve command is run', function () {
    $this->artisan('manipulate-image')
         ->expectsQuestion('What would you like to do?', 'Retrieve an Image')
         ->assertExitCode(0);

    $this->assertCommandCalled('manipulate-image:retrieve');
});

test('delete command is run', function () {
    $this->artisan('manipulate-image')
         ->expectsQuestion('What would you like to do?', 'Delete an Image')
         ->assertExitCode(0);

    $this->assertCommandCalled('manipulate-image:delete');
});
