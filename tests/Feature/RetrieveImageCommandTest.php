<?php

$sourceImageLocation          = '/var/www/html/storage/tests/images/TestImage.png';
$testImageLocation            = '/var/www/html/storage/tests/images/TempTestImage.png';
$testDestinationImageLocation = '/var/www/html/storage/tests/images/NewTempTestImage.png';

// Create temporary PNG file
beforeAll(function () use ($sourceImageLocation, $testImageLocation) {
    copy($sourceImageLocation, $testImageLocation);
});

// Delete temporary files
afterAll(function () use ($testImageLocation, $testDestinationImageLocation) {
    if (file_exists($testImageLocation)) {
        unlink($testImageLocation);
    }
    if (file_exists($testDestinationImageLocation)) {
        unlink($testDestinationImageLocation);
    }
});

it('can retrieve and keep original image', function () use ($testImageLocation, $testDestinationImageLocation) {
    $this->artisan('manipulate-image:retrieve')
         ->expectsQuestion('Please supply the location of the file you want to retrieve.', $testImageLocation)
         ->expectsQuestion('Please supply the destination location of the file you want to retrieve.',
             $testDestinationImageLocation)
         ->expectsConfirmation('Do you want to keep the original version of the file?', 'yes')
         ->assertExitCode(0);

    $this->assertFileExists($testImageLocation);
    $this->assertFileExists($testDestinationImageLocation);
});

it('can retrieve and delete original image', function () use ($testImageLocation, $testDestinationImageLocation) {
    $this->artisan('manipulate-image:retrieve')
         ->expectsQuestion('Please supply the location of the file you want to retrieve.', $testImageLocation)
         ->expectsQuestion('Please supply the destination location of the file you want to retrieve.',
             $testDestinationImageLocation)
         ->expectsConfirmation('Do you want to keep the original version of the file?', 'no')
         ->assertExitCode(0);

    $this->assertFileDoesNotExist($testImageLocation);
    $this->assertFileExists($testDestinationImageLocation);
});
