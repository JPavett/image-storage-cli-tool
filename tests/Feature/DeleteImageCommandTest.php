<?php

$sourceImageLocation = '/var/www/html/storage/tests/images/TestImage.png';
$testImageLocation   = '/var/www/html/storage/tests/images/TempTestImage.png';

// Delete temporary files
afterAll(function () use ($testImageLocation) {
    if (file_exists($testImageLocation)) {
        unlink($testImageLocation);
    }
});

it('can delete image', function () use ($sourceImageLocation, $testImageLocation) {

    copy($sourceImageLocation, $testImageLocation);

    $this->artisan('manipulate-image:delete')
         ->expectsQuestion('Please supply the location of the file you want to delete.', $testImageLocation)
         ->assertExitCode(0);

    $this->assertFileDoesNotExist($testImageLocation);
});
