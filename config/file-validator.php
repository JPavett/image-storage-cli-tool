<?php

return [
    // The allowed image MIME types for manipulation
    'allowed_image_mime_types' => [
        'image/gif',
        'image/jpeg',
        'image/tiff',
        'image/bmp',
        'image/png',
    ],
];
