<?php

return [
    // The storage adapter to be used when manipulating images
    'storage_adapter' => \App\Adapters\LocalAdapter::class
];
