<?php

namespace App;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

class FileLogger implements LoggerInterface
{
    use LoggerTrait;

    /**
     * @var string
     */
    private string $logFile;

    public function __construct(string $logFile)
    {
        $this->logFile = $logFile;

        if ( ! file_exists($logFile)) {
            touch($logFile);
        }
    }

    /**
     * Format the log line.
     *
     * @param $level
     * @param  \Stringable|string  $message
     * @param  array  $context
     *
     * @return string
     */
    public function format($level, \Stringable|string $message, array $context = [])
    : string {
        return date('Y-m-d H:i:s').' '.strtoupper($level).' '.$message."\n";
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param $level
     * @param  \Stringable|string  $message
     * @param  array  $context
     *
     * @return void
     */
    public function log($level, \Stringable|string $message, array $context = [])
    : void {
        $log = $this->format($level, $message, $context);
        file_put_contents($this->logFile, $log, FILE_APPEND | LOCK_EX);
    }
}
