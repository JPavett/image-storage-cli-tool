<?php

namespace App\Providers;

use App\FileLogger;
use App\Filesystem;
use App\Interfaces\FilesystemAdapter;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FilesystemAdapter::class, function ($app) {
            return new $app['config']['filesystem.storage_adapter'];
        });

        $this->app->singleton(Filesystem::class, function () {
            return new Filesystem(app(FilesystemAdapter::class));
        });

        $this->app->singleton(LoggerInterface::class, function ($app) {
            return new FileLogger($app['config']['logger.default_log_file']);
        });
    }
}
