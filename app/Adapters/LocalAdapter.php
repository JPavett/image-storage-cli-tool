<?php

namespace App\Adapters;

use App\Interfaces\FilesystemAdapter;

class LocalAdapter implements FilesystemAdapter
{
    /**
     * @param  string  $path
     *
     * @return bool
     */
    public function fileExists(string $path)
    : bool {
        return file_exists($path);
    }

    /**
     * @param  string  $source
     * @param  string  $destination
     *
     * @return bool
     */
    public function move(string $source, string $destination)
    : bool {
        if ($this->fileExists($source)) {
            return rename($source, $destination);
        } else {
            return false;
        }
    }

    /**
     * @param  string  $source
     * @param  string  $destination
     *
     * @return bool
     */
    public function copy(string $source, string $destination)
    : bool {
        if ($this->fileExists($source)) {
            return copy($source, $destination);
        } else {
            return false;
        }
    }

    /**
     * @param  string  $path
     *
     * @return bool
     */
    public function delete(string $path)
    : bool {
        if ($this->fileExists($path)) {
            return unlink($path);
        } else {
            return false;
        }
    }
}
