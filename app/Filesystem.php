<?php

namespace App;

use App\Interfaces\FilesystemAdapter;

class Filesystem
{
    /**
     * @var \App\Interfaces\FilesystemAdapter
     */
    private FilesystemAdapter $filesystemAdapter;

    /**
     * @param  \App\Interfaces\FilesystemAdapter  $filesystemAdapter
     */
    public function __construct(FilesystemAdapter $filesystemAdapter)
    {
        $this->filesystemAdapter = $filesystemAdapter;
    }

    /**
     * Check if a file exists at the specified location.
     *
     * @param  string  $filePath
     *
     * @return bool
     */
    public function fileExists(string $filePath)
    {
        return $this->filesystemAdapter->fileExists($filePath);
    }

    /**
     * Move a specified file to a new location.
     *
     * @param  string  $sourceLocation
     * @param  string  $destinationLocation
     *
     * @return bool
     */
    public function moveFile(string $sourceLocation, string $destinationLocation)
    {
        return $this->filesystemAdapter->move($sourceLocation, $destinationLocation);
    }

    /**
     * Copy a specified file to a new location.
     *
     * @param  string  $sourceLocation
     * @param  string  $destinationLocation
     *
     * @return bool
     */
    public function copyFile(string $sourceLocation, string $destinationLocation)
    {
        return $this->filesystemAdapter->copy($sourceLocation, $destinationLocation);
    }

    /**
     * Delete a specified file.
     *
     * @param  string  $filePath
     *
     * @return bool
     */
    public function deleteFile(string $filePath)
    {
        return $this->filesystemAdapter->delete($filePath);
    }
}
