<?php

namespace App\Interfaces;

interface FilesystemAdapter
{
    /**
     * @param  string  $path
     *
     * @return bool
     */
    public function fileExists(string $path): bool;

    /**
     * @param  string  $source
     * @param  string  $destination
     *
     * @return bool
     */
    public function move(string $source, string $destination): bool;

    /**
     * @param  string  $source
     * @param  string  $destination
     *
     * @return bool
     */
    public function copy(string $source, string $destination): bool;

    /**
     * @param  string  $path
     *
     * @return bool
     */
    public function delete(string $path): bool;
}
