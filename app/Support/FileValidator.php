<?php

namespace App\Support;

use App\Filesystem;
use App\FileLogger;
use LaravelZero\Framework\Commands\Command;

class FileValidator
{
    /**
     * @var array
     */
    private array $allowedImageMimeTypes;

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct()
    {
        $this->allowedImageMimeTypes = app('config')->get('file-validator.allowed_image_mime_types');
    }

    /**
     * Check if image is an allowed MIME type.
     *
     * @param  string  $filePath
     *
     * @return bool
     */
    public function isAllowedImageType(string $filePath){
        return in_array(mime_content_type($filePath), $this->allowedImageMimeTypes);
    }
}
