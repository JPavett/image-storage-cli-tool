<?php

namespace App\Commands;

use App\Support\FileValidator;
use LaravelZero\Framework\Commands\Command;

abstract class BaseImageCommand extends Command
{
    /**
     * Check if the file is valid and an image.
     *
     * @param  string  $location
     *
     * @return bool
     */
    protected function isValidImageFile(string $location)
    : bool {
        if ( ! file_exists($location)) {
            $this->error('Not a valid file path.');

            return false;
        }

        if ( ! (new FileValidator())->isAllowedImageType($location)) {
            $this->error('Not an allowed image type.');

            return false;
        }

        return true;
    }
}
