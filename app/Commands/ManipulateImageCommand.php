<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;

class ManipulateImageCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'manipulate-image';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Run the top-level manipulate image command';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $string  = "What would you like to do?";
        $options = ['Store an Image', 'Retrieve an Image', 'Delete an Image'];

        $input = $this->choice($string, $options);

        switch ($input) {
            case $options[0]:
                $this->call('manipulate-image:store');
                break;
            case $options[1]:
                $this->call('manipulate-image:retrieve');
                break;
            case $options[2]:
                $this->call('manipulate-image:delete');
                break;
        }

        return 0;
    }
}
