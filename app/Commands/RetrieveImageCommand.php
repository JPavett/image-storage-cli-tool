<?php

namespace App\Commands;

use App\Filesystem;
use Psr\Log\LoggerInterface;

class RetrieveImageCommand extends BaseImageCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'manipulate-image:retrieve';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Retrieve an Image';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $sourceLocation      = $this->getImageLocation();
            $destinationLocation = $this->ask('Please supply the destination location of the file you want to retrieve.');

            if ($this->confirm('Do you want to keep the original version of the file?')) {
                $stored = $this->storeImage($sourceLocation, $destinationLocation);
            } else {
                $stored = $this->moveImage($sourceLocation, $destinationLocation);
            }

            if ($stored) {
                $this->info('File Stored Successfully');
                app(LoggerInterface::class)->info('File retrieved successfully from '.$sourceLocation);
            } else {
                $this->info('Unable to Store File');
                app(LoggerInterface::class)->info('Unable to retrieve file from '.$sourceLocation);

                return 1;
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            app(LoggerInterface::class)->info('Exception when retrieving file. '.$e->getMessage());

            return 1;
        }

        return 0;
    }

    /**
     * Get the location of the file
     *
     * @return string
     */
    private function getImageLocation()
    {
        $location = $this->ask('Please supply the location of the file you want to retrieve.');

        if ($this->isValidImageFile($location)) {
            return $location;
        } else {
            return $this->getImageLocation();
        }
    }

    /**
     * Store the image on the filesystem
     *
     * @return bool
     */
    public function storeImage($sourceLocation, $destinationLocation)
    {
        return app(Filesystem::class)->copyFile($sourceLocation, $destinationLocation);
    }

    /**
     * Move the image on the filesystem
     *
     * @return bool
     */
    public function moveImage($sourceLocation, $destinationLocation)
    {
        return app(Filesystem::class)->moveFile($sourceLocation, $destinationLocation);
    }
}
