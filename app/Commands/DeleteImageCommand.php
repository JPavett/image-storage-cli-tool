<?php

namespace App\Commands;

use App\Filesystem;
use Psr\Log\LoggerInterface;

class DeleteImageCommand extends BaseImageCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'manipulate-image:delete';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Delete an image';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $location = $this->getImageLocation();

            if ($this->deleteImage($location)) {
                $this->info('File Deleted Successfully');
                app(LoggerInterface::class)->info('File deleted successfully at '.$location);
            } else {
                $this->info('Unable to Delete File');
                app(LoggerInterface::class)->info('Unable to delete file at '.$location);
            }
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            app(LoggerInterface::class)->info('Exception when deleting file. '.$e->getMessage());

            return 1;
        }

        return 0;
    }

    /**
     * Get the location of the file
     *
     * @return string
     */
    private function getImageLocation()
    {
        $location = $this->ask('Please supply the location of the file you want to delete.');

        if ($this->isValidImageFile($location)) {
            return $location;
        } else {
            return $this->getImageLocation();
        }
    }

    /**
     * Delete the image from the filesystem
     *
     * @return bool
     */
    private function deleteImage($location)
    {
        return app(Filesystem::class)->deleteFile($location);
    }
}
