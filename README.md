<h4><center>Image Storage CLI Tool</center></h4>

This is an image storage tool written in PHP 8.1.1. The Laravel Zero CLI microframework is used to supply all console functionality.

A PSR-3 file logger is also intergrated into the application, instead of the avaliable Laravel solution.




## Build Application

The application is containerised, it can be built with the following command:

`docker-compose up -d`


## Running Commands
Five commands are avaliable within the application:


`docker-compose exec app php applicaton`

To list all avaliable commands


`docker-compose exec app php applicaton manipulate-image`

To run the top level command


`docker-compose exec app php applicaton manipulate-image:store`

To store a file in a specified location


`docker-compose exec app php applicaton manipulate-image:retrieve`

To retrieve a file from a specified location


`docker-compose exec app php applicaton manipulate-image:delete`

To delete a file in a specified location

## Running Commands

All unit and feature tests can be run with the following command:

`docker-compose exec app pest`

## Future Work

- [ ] Optimize Docker configuration
- [ ] Allowed additional logger types beyond File
- [ ] Allow splitting of log files
- [ ] Enhanced how Exception stack traces are logged
